# Tech
- [express js](https://expressjs.com/)
- [fastest-validator](https://github.com/icebob/fastest-validator)
- [pg-promise](https://github.com/vitaly-t/pg-promise)
- [sequelize](https://sequelize.org/docs/v6/)
- [pino](https://github.com/pinojs/pino)
- [mocha](https://mochajs.org/)
- [swagger-jsdoc](https://github.com/Surnet/swagger-jsdoc)
- [swagger-ui-express](https://github.com/scottie1984/swagger-ui-express)
# Run Server
1. $ npm i
2. $ cp .env.example .env
4. create `log` folder in the root (production only)
5. $ npm run dev
## Sequelize 
### Create Database
$ npx sequelize db:create
### Create Model & Migration File
$ npx sequelize model:generate --name <table_name> --attributes <column_name>:<column_type>,<column_name>:<column_type> --models-path <dir_path>
### Migration
#### Migration skeleton
$ npx sequelize migration:generate --name migration-skeleton
#### Run migration
$ npx sequelize db:migrate
#### Undo migrtion
$ npx sequelize db:migrate:undo
### Seeder
#### Generate seeder
$ npx sequelize seed:generate --name seeder-name
#### Run seeder
$ npx sequelize db:seed:all
#### Undo seeder
- $ npx sequelize db:seed:undo
- $ npx sequelize db:seed:undo --seed name-of-seed-as-in-data