const app = require('../../../../index')
const chai = require('chai');
const chaiHttp = require('chai-http');
const { assert } = require('chai');

const pathRegister = '/api/user/register'
const pathUserDetail = '/api/user/1'
const pathUserInvalid = '/api/user/0'

const should = chai.should();
chai.use(chaiHttp);

describe('Route User', function () {
  describe(`POST ${pathRegister}`, function () {
    it('should return validation error', (done) => {
      chai.request(app)
        .post(pathRegister)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  
    it('should return success', (done) => {
      chai.request(app)
        .post(pathRegister)
        .send({
          'name': 'User',
          'email': 'user@example.com',
          'password': '123123',
          'password_confirm': '123123'
        })
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  });
  
  describe(`GET /api/user/:id`, function () {
    it('should return user null', (done) => {
      chai.request(app)
        .get(pathUserInvalid)
        .end((err, res) => {
          res.should.have.status(200);
          assert.equal(res.body.user, null)
          done();
        });
    });
  
    it('should return user detail', (done) => {
      chai.request(app)
        .get(pathUserDetail)
        .end((err, res) => {
          res.should.have.status(200);
          assert.notEqual(res.body.user, null)
          done();
        });
    });
  });
})
