require('dotenv').config();
const { version } = require('../package.json');

module.exports = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'NodeApp',
      version,
    },

    servers: [
      {
        url: `http://${process.env.SERVER_HOST}:${process.env.SERVER_PORT}`,
        description: 'My API Documentation',
      },
    ],
  },
  apis: ['./docs/*.yml', './modules/**/routes/*.js', './index.js'],
}