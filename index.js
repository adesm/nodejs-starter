require('dotenv').config();
require('express-group-routes');
require('module-alias/register');

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser')
const Logger = require('./utils/logger');
const routeApi = require('./routes/api');
const swaggerJsdoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const specs = swaggerJsdoc(require('./docs/option'));

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

if (process.env.NODE_ENV !== 'test') {
  app.use(Logger.http);
}

app.use('/docs', swaggerUi.serve, swaggerUi.setup(specs));

app.get('/', (req, res) => {
  res.send('Hello node');
})

app.group('/api', (router) => {
  routeApi(router);
});

const config = {
  host: process.env.SERVER_HOST || 'localhost',
  port: process.env.SERVER_PORT || 3000,
}
module.exports = app.listen(config);