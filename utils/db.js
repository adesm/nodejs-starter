const Logger = require('./logger')
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('@root/database/config/config')[env];

const initOptions = {
  error(err, e) {
    let type = ''
    if (e.cn) {
      type = 'connection'
      // this is a connection-related error
      // cn = safe connection details passed into the library:
      //      if password is present, it is masked by #
    }

    if (e.query) {
      type = 'query'
      // query string is available
      if (e.params) {
        // query parameters are available
      }
    }

    if (e.ctx) {
      type = 'transaction'
      // occurred inside a task or transaction
    }
    Logger.internal.error(err, `Database error: ${type}`);
  }
};
const pgp = require('pg-promise')(initOptions);

module.exports = pgp(dbConfig)

