require('dotenv').config();

const isProd = process.env.NODE_ENV === 'production'
const date = new Date();
const fileName = `./logs/core-system-${date.toISOString().substring(0, 10)}.log`;
const fileNameHttp = `./logs/core-http-${date.toISOString().substring(0, 10)}.log`;
const prettyPrint = !isProd ? {
  colorize: true,
  translateTime: 'yyyy-mm-dd HH:MM:ss Z',
  singleLine: !isProd,
} : null

const internalLogger = require('pino')({
  name: 'Log System',
  level: process.env.PINO_LOG_LEVEL || 'info',
  prettyPrint: prettyPrint
}, isProd ? fileName : null);

const httpLogger = require('pino')({
  name: 'Log Http Request',
  level: process.env.PINO_LOG_LEVEL || 'info',
  prettyPrint: prettyPrint
}, isProd ? fileNameHttp : null);

const expressPino = require('express-pino-logger')({
  logger: httpLogger,
});


module.exports = {
  http: expressPino,
  internal: internalLogger
};
