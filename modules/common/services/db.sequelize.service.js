const { Sequelize } = require('sequelize');
const Logger = require('@utils/logger')
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('@root/database/config/config')[env];

class DBSequelizeService {
  constructor() {
    this.sequelize = new Sequelize({
      ...dbConfig,
      logging: (sql, timing) => Logger.internal.info(sql, typeof timing === 'number' ? `Elapsed time: ${timing}ms` : ''),
    });

    this.connect()    
  }

  async connect() {
    try {
      await this.sequelize.authenticate();
    } catch (error) {
      Logger.internal.error(error, `Database sequelize connection error`);
    }
  }

  getInstance() {
    return this.sequelize
  }

  generateModel(name, config) {
    return this.sequelize.define(name, config);
  }
}

module.exports = DBSequelizeService