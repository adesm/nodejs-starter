class DBCommonService {
  constructor() {
    this.db = require('@utils/db');
  }

  // return multiple rows
  async any(query, data = []) {
    try {
      const result = await this.db.any(query, data)
      return result
    } catch (ex) {
      return { errorCode: ex.code };
    }

  }

  // return one row
  async one(query, data = []) {
    try {
      const result = await this.db.one(query, data)
      return result
    } catch (ex) {
      return { errorCode: ex.code };
    }

  }
}

module.exports = DBCommonService

