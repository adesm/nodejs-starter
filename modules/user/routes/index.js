const UserController = require('@module-user/controllers/user.controller');
const auth = require('@middleware/auth')

module.exports = (app) => {
  const userController = new UserController();

  app.group('/user', (router) => {
    router.route('/register').post(userController.registerUser)
  });

  app.group('/user', (router) => {
    router.use(auth);
    router.route('/:id')
      .get(userController.getUser);
  });
};

/**
 * @swagger
 * tags:
 *   name: User
 *   description: User Modules
 */

/**
 * @swagger
 * /api/user/register:
 *   post:
 *     summary: Register as user
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - email
 *               - password
 *             properties:
 *               name:
 *                 type: string
 *               email:
 *                 type: string
 *                 format: email
 *                 description: must be unique
 *               password:
 *                 type: string
 *                 format: password
 *                 minLength: 6
 *                 description: At least one number and one letter
 *               password_confirm:
 *                 type: string
 *                 format: password
 *                 minLength: 6
 *                 description: same with password
 *             example:
 *               name: fake name
 *               email: fake@example.com
 *               password: password1
 *               password_confirm: password1
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 user:
 *                   $ref: '#/components/schemas/User'
 */
