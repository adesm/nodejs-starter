const UserService = require('@module-user/services/user.service');

class UserController {
  constructor() {
    this.userService = new UserService();
    this.getUser = this.getUser.bind(this);
    this.registerUser = this.registerUser.bind(this)
  }

  async registerUser(req, res) {
    const result = await this.userService.registerUser(req.body);
    const hasErrorCode = Object.prototype.hasOwnProperty.call(result, 'error');

    res.status(result.httpCode);
    if (hasErrorCode) {
      res.send(result.error);
    } else {
      res.send(result.data);
    }
  }

  async getUser(req, res) {
    const result = await this.userService.getUserById(req.params.id);
    const hasErrorCode = Object.prototype.hasOwnProperty.call(result, 'error');

    res.status(result.httpCode);
    if (hasErrorCode) {
      res.send(result.error);
    } else {
      res.send(result.data);
    }
  }
}

module.exports = UserController;
