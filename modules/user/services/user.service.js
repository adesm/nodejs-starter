// const Logger = require('@lib-logger/internal.logger');
const HttpStatusCode = require('http-status-codes');
const UserModel = require('@module-user/models/user.model');
const UserValidator = require('@module-user/services/user.validator.service')

class UserService {
  constructor() {
    this.userModel = new UserModel();
  }

  async registerUser(data) {
    const isValidData = UserValidator.validateRegister(data);

    if (isValidData !== true) {
      return isValidData;
    }

    return {
      httpCode: HttpStatusCode.StatusCodes.CREATED,
      data: {
        user: data,
        tokens: null
      }
    }
  }

  async getUserById(id) {
    const result = await this.userModel.getUserById(id)

    if (result.errorCode === undefined) {
      if (result.length === 0) {
        return {
          httpCode: HttpStatusCode.StatusCodes.OK,
          data: {
            user: null
          }
        };
      }

      const user = result[0];

      return {
        httpCode: HttpStatusCode.StatusCodes.OK,
        data: {
          user: user
        },
      };
    }

    return {
      httpCode: HttpStatusCode.StatusCodes.INTERNAL_SERVER_ERROR,
      error: {
        error_code: 'INTERNAL_SERVER_ERROR',
        message: 'Internal server error',
      },
    };
  }
}

module.exports = UserService;
