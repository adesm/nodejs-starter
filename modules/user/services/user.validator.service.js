const validator = require('@common/services/validator.common.service')
const HttpStatusCode = require('http-status-codes');

class UserValidatorService {
  static validateRegister(data) {
    const schema = {
      name: { type: 'string', min: 3 },
      email: { type: 'email' },
      password: 'string|min:6',
      password_confirm: { type: 'equal', field: 'password' }
    };
  
    const isValidForm = validator.validate(schema, data);

    if (isValidForm !== true) {
      return {
        httpCode: HttpStatusCode.StatusCodes.BAD_REQUEST,
        error: {
          error_code: 'REGISTER_VALIDATION_ERROR',
          error: isValidForm,
          message: 'Register validation error',
        },
      };
    }

    return true

  }
}

module.exports = UserValidatorService