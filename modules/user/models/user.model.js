const DBService = require('@common/services/db.common.service')
const DBSequelizeService = require('@common/services/db.sequelize.service')
const { DataTypes } = require('sequelize')

class UserModel {
  constructor() {
    this.dBService = new DBService();
    this.tableName = 'users';

    // using sequelize
    // const sequelize = new DBSequelizeService().getInstance();
    // this.User = sequelize.define('users', {
    //   // Model attributes are defined here
    //   email: {
    //     type: DataTypes.STRING,
    //   }
    // }, {
    //   // Other model options go here
    //   underscored: true
    // });
  }

  async getUserById(id) {
    const query = `
                SELECT  *
                FROM ${this.tableName}
                WHERE id = $1`;

    const result = await this.dBService.any(query, id);

    return result;
  }
  
  // sequelize query method
  // async findById(id) {
  //   try {
  //     const user = await this.User.findByPk(id);
  //     return user
  //   } catch (error) {
  //     return { errorCode: 'DB_ERROR' };
  //   }

  // }
}

module.exports = UserModel